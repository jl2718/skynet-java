/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author johnlakness
 */

import com.google.maps.GeoApiContext;
import com.google.maps.ElevationApi;
import com.google.maps.model.LatLng;
import com.google.maps.model.ElevationResult;
        
//import dji.sdk.Gimbal.DJIGimbal.DJIGimbalState;
import dji.sdk.Gimbal.DJIGimbal.DJIGimbalAttitude;
//import dji.sdk.FlightController.DJIFlightControllerDataType.DJIFlightControllerCurrentState;
import dji.sdk.FlightController.DJIFlightControllerDataType.DJILocationCoordinate3D;
import dji.sdk.FlightController.DJIFlightControllerDataType.DJIAttitude;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;

public class Locator {
    private GeoApiContext context;
    public Locator(String apikey){
        this.context  = new GeoApiContext().setApiKey(apikey);
    }
    public DJILocationCoordinate3D LocatePixel(
            DJILocationCoordinate3D location,
            DJIAttitude heading,
            DJIGimbalAttitude pointing,
            int pixel_x,
            int pixel_y
            ){
        // not currently implemented
        return (new DJILocationCoordinate3D(0,0,0));
    }
    public DJILocationCoordinate3D LocateCenter(
            DJILocationCoordinate3D location,
            DJIAttitude heading,
            DJIGimbalAttitude pointing
            ){
        // 1. get pointing vector 
        Matrix3d rotation_matrix;
        rotation_matrix = this.getRotationMatrix(heading.pitch+pointing.pitch,
                                                 heading.roll+pointing.roll,
                                                 heading.yaw+pointing.yaw); // I think this works....
        Vector3d pointing_vector;
        pointing_vector = new Vector3d();
        pointing_vector.setY(1);
        rotation_matrix.transform(pointing_vector);

        // 2. get intersection with plane using reported altitude
        Vector3d plane_origin,line_origin,plane_normal,target_offset;
        plane_origin = new Vector3d();
        plane_origin.setZ(-1.0*location.getAltitude());
        plane_normal = new Vector3d();
        plane_normal.setZ(1);
        line_origin = new Vector3d();
        target_offset = this.PlaneIntersect(line_origin, pointing_vector, plane_origin, plane_normal);
        DJILocationCoordinate3D target_global;
        target_global = this.LocalToGlobal(location,target_offset);
        // 2a. Optimize using elevation API (skipping this for now)
        LatLng[] points;
        points = new LatLng[2];
        points[0] = new LatLng(location.getLatitude(),location.getLongitude());
        points[1] = new LatLng(target_global.getLatitude(),target_global.getLongitude());
        ElevationResult[] elevations;
        try{
            elevations = ElevationApi.getByPoints(context,points).await();
        }catch(Exception e){
            System.out.println(e);
            elevations = new ElevationResult[points.length];
            for (int i=0;i<points.length;i++){
                elevations[i].location=points[i];
                elevations[i].elevation=location.getAltitude();
            }
            
        }
        Vector3d pplane,tline;
        pplane = new Vector3d();
        pplane.cross(pointing_vector, new Vector3d(0,0,1));
        pplane.normalize();
        plane_origin.setZ(-1.0*elevations[0].elevation);
        tline = new Vector3d();
        tline.sub(
                plane_origin,
                new Vector3d(target_offset.x,target_offset.y,-1.0*elevations[1].elevation)
        );
        tline.normalize();
        plane_normal.cross(pplane, tline);
        plane_normal.normalize();
        target_offset = this.PlaneIntersect(line_origin, pointing_vector, plane_origin, plane_normal);
        target_global = this.LocalToGlobal(location,target_offset);
        // 3. covert from local frame to global lat/lon and return
        return target_global;
    } 
    private DJILocationCoordinate3D LocalToGlobal(DJILocationCoordinate3D location,Vector3d offset){
        double earth_diameter = 12.742e6;
        double lat_meters,lon_meters;
        lat_meters = earth_diameter*Math.PI/360.0;
        lon_meters = lat_meters*Math.cos(location.getLatitude());
        return (new DJILocationCoordinate3D(location.getLatitude()+offset.x/lat_meters,location.getLongitude()+offset.y/lon_meters,location.getAltitude()+(float)offset.z));
    }
    private Matrix3d getRotationMatrix(double pitch,double roll,double yaw){
        // This is total crap if the rotations are internal angles as in real gimbal dynamics
        Matrix3d P,R,Y,X;
        P = new Matrix3d();
        P.rotX(pitch);
        R = new Matrix3d();
        R.rotY(roll);
        Y = new Matrix3d();
        Y.rotZ(yaw);
        X = new Matrix3d();
        X.mul(P,R);
        X.mul(Y);
        return X;
    }
    private Vector3d PlaneIntersect(
            Vector3d line_origin,
            Vector3d line_direction,
            Vector3d plane_origin,
            Vector3d plane_normal
            ){
        Vector3d a,b,c;
        double d,e;
        
        a = new Vector3d();
        a.sub(plane_origin,line_origin);
        d=a.dot(plane_normal);
        b = new Vector3d(line_direction);
        e=b.dot(plane_normal); 
        c = new Vector3d();
        c.scale(d/e,line_direction);
        c.add(line_origin);
        return c;
    }
}

